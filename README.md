
- bin/sh: 存放构建脚本，或者其他类型的shell脚本.
- src: 后端服务源码
- tests: 单元测试用例

## 单元测试覆盖率

```bash
ls
# 生成虚拟环境
python3 -m venv venv
# 激活虚拟环境
source venv/bin/activate
# 安装相关环境
pip install -r requirements.txt
# 单元测试
pytest --cov=src tests --cov-report=html 
#pytest --cov=src tests --cov-report=html --cov-fail-under=90
```

### 这是我今天开发的代码