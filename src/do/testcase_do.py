from sqlalchemy import Column, Integer, String

from src.server import Base, engine


class TestcaseDo(Base):
    __tablename__ = "testcase"
    id = Column(Integer, primary_key=True)
    name = Column(String(100), default=None, nullable=False, comment="用例名称")
    remark = Column(String(100), default=None, comment="备注")

    def as_dict(self):
        return {"id": self.id, "name": self.name, "remark": self.remark}

#
# if __name__ == '__main__':
#     Base.metadata.create_all(engine)
