from flask import Blueprint, request

from src.do.testcase_do import TestcaseDo
from src.service.testcase_service import TestcaseService
from src.utils.log_utils import logger

testcase = Blueprint('testcase', __name__, url_prefix="/testcase")
testcase_service = TestcaseService()



@testcase.route("")
def get_testcase():
    # 问题： 返回了一个对象类型
    testcase_obj_list = testcase_service.list()
    json_list = [i.as_dict() for i in testcase_obj_list]
    return {"code": 1, "data": json_list}


@testcase.route("", methods=["post"])
def post_testcase():
    testcase_do = TestcaseDo(**request.json)
    testcase_id = testcase_service.create(testcase_do)
    return {"code": 1, "message": f"testcase {testcase_id} create success"}


@testcase.route("", methods=["delete"])
def delete_testcase():
    # url 参数信息
    delete_id = request.args.get("id")
    deleted_id = testcase_service.delete(delete_id)
    return {"code": 1, "message": f"testcase {deleted_id} delete success"}



@testcase.route("", methods=["put"])
def update_testcase():
    update_info:dict = request.json
    testcase_id = update_info.get("id")
    logger.debug(f"需要更新的用例为{update_info}")
    updated_id = testcase_service.update(testcase_id, update_info)
    return {"code": 1, "message": f"testcase {updated_id} update success"}

