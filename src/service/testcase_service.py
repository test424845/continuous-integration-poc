from typing import List

from src.do.testcase_do import TestcaseDo
from src.server import db_session


class TestcaseService:
    def get(self, testcase_id):
        """
        根据测试用例的id 获取测试用例信息
        :return:
        """
        return db_session.query(TestcaseDo).filter_by(id=testcase_id).first()

    def list(self)-> List[TestcaseDo]:
        """
        获取所有的测试用例
        :return:
        """
        return db_session.query(TestcaseDo).all()

    def create(self, testcase_do: TestcaseDo):
        """
        新建测试用例
        :return:
        """
        db_session.add(testcase_do)
        db_session.commit()
        testcase_id = testcase_do.id
        return testcase_id


    def update(self, testcase_id, update_testcase: dict):
        """
        更新测试用例
        :return:
        """
        db_session.query(TestcaseDo)\
            .filter_by(id=testcase_id)\
            .update(update_testcase)
        db_session.commit()
        return testcase_id

    def delete(self, testcase_id):
        """
        根据用例id删除测试用例
        :param testcase_id:
        :return:
        """
        testcase_do = db_session.query(TestcaseDo).filter_by(id=testcase_id).first()
        db_session.delete(testcase_do)
        db_session.commit()
        return testcase_id
