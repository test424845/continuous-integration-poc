import os
import sys
from flask import Flask, redirect
from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base, sessionmaker, Session
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from src import project_path

# 2. 创建Flask应用程序实例
app = Flask(__name__)

@app.route("/")
def index():
    return redirect("/testcase")

# 先创建基类，使用declarative_base()方法声明一个基类
# 创建表的时候需要继承这个基类对象
Base = declarative_base()
db_host = "localhost"  # MySQL主机名
db_port = "3306"       # MySQL端口号，默认3306
db_name = "testplatform" # 数据库名称
db_user = "root"   # 数据库用户名
db_pass = "123456"   # 数据库密码
# 数据库类型+数据库引擎（ pip install pymysql）
# db_url = f"mysql+pymysql://{db_user}:{db_pass}@{db_host}:{db_port}/{db_name}"


db_url = f'sqlite:///{project_path}/db/data.db'

# 创建引擎，连接到数据库
engine = create_engine(db_url)
# 创建session对象
DBSession = sessionmaker(bind=engine)
db_session: Session = DBSession()

def register_router():
    from src.router.testcase_router import testcase
    app.register_blueprint(testcase)



if __name__ == '__main__':
    register_router()
    app.run(debug=True, host="0.0.0.0")
